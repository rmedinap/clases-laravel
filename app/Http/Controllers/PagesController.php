<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function home() {
		$people = ['uno','dos','tres']; // puedo eliminar estas lineas del route.php
		return view('vista')->withPeople($people); 
	}
}
