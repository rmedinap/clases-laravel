<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Card;

use App\Http\Requests;

class CardsController extends Controller
{
	public function home()
	{
		$cards = Card::all();
		return view('cards.index', compact('cards'));
	}
	
	public function show(Card $card) {
		$card ->load('notes.user');
		// otra forma:
		// $card = Card::with('notes')->get();
		// $card = $card[0];
		//return $card;
		return view('cards.show', compact('card'));
	}
}
