<body>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	@yield('header')
</head>
	@yield('content')

<footer>
	@yield('footer')
</footer>
</body>