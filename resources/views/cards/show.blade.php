@extends('layout')
@section('content')
	{{ $card->title }}

	<ul>
	@foreach ($card->notes as $note)
		<li>{{ $note->body }} (<a href="#" >{{ $note->user->name }}</a>)</li>
	@endforeach
	</ul>


<h3>Add new note</h3>
<form method="POST" action="/cards/{{ $card->id }}/notes">

	<input type="hidden" name="user_id" value="{{ $card->user_id }}">
	<div class="form-group">
	<textarea name="body" class="form-control"></textarea>
	</div>
	<div class="form-group">
	<button type="submit" class="btn btn-primaryl">Add note</button>
	</div>
</form>

@stop